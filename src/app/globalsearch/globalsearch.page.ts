import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-globalsearch',
  templateUrl: './globalsearch.page.html',
  styleUrls: ['./globalsearch.page.scss'],
})
export class GlobalsearchPage implements OnInit {
  id: any;
  values:any={};
  constructor(private route: ActivatedRoute) { }
  
  ngOnInit() {
    debugger;
    this.route.queryParams.subscribe(params => {
      this.id = params["id"];
      console.log(this.id);
  });
  }

}
