import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoadingController,NavController } from '@ionic/angular';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { GlobalsearchPage } from '../globalsearch/globalsearch.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  isLoading = false;
  constructor(
    private barcodeScanner: BarcodeScanner,
    private  http:HttpClient,
    public loadingController: LoadingController,
    private uniqueDeviceID: UniqueDeviceID,
    public navCtrl: NavController) {}
  data:any={};
  scan() { 
    
  this.barcodeScanner.scan().then(barcodeData => {
    console.log('Barcode data', barcodeData);
    this.getById(barcodeData.text);
   }).catch(err => {
       console.log('Error', err);
   });
   
}

  getById(id:string){
this.present();
    let apicall = "https://funckivanc.azurewebsites.net/api/items/"+id+"";

    var result=this.http.get(apicall);
    
    result
    .subscribe(res => {
      if((<Array<Object>>res).length===0){
        console.log(id);
        var jsondata ={};
        jsondata["id"]=id;
        this.data["id"]=id;
      }
      else{
      this.data=res[0];
      }
    },(err)=>{
      console.log('error');

    })
    this.dismiss();
  }

  addOrUpdate() {
    this.present();
    var displayDate = new Date().toLocaleString();
    let item:any={};
    item["ModifiedDate"]=displayDate;
    item["id"]=this.data["id"];
    item["name"]=this.data["name"];
    item["brand"]=this.data["brand"];
    item["type"]=this.data["type"];
    let headers: HttpHeaders = new HttpHeaders();
    headers.append("Access-Control-Allow-Origin", "*");
    this.http.post('https://funckivanc.azurewebsites.net/api/items', item,{headers:headers})
        .subscribe(res => {
          
        }, (err) => {
          console.log('error');
          alert(err.message);
        }
      );
      let cartitem:any={};
      cartitem["uid"]=this.uniqueDeviceID.get();
      cartitem["itemid"]=this.data["id"];
      cartitem["createdDate"]=this.data["ModifiedDate"];
      cartitem["price"]=this.data["price"];
      cartitem["store"]=this.data["store"];
      this.http.post('https://funckivanc.azurewebsites.net/api/cartitem', cartitem,{headers:headers})
        .subscribe(res => {
          
        }, (err) => {
          console.log('error');
          alert(err.message);
        }
      );

      this.dismiss();
  }

   newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });

    
}

globalsearch(id:string) {
  this.navCtrl.navigateForward(['globalsearch',{'id':id}]);
}

mysearch(id:string) {
  this.navCtrl.navigateForward(['globalsearch',{'id':id}]);
}

graph(id:string) {
  this.navCtrl.navigateForward(['globalsearch',{'id':id}]);
}

async present() {
  this.isLoading = true;
  return await this.loadingController.create({
    duration: 5000,
  }).then(a => {
    a.present().then(() => {
      console.log('presented');
      if (!this.isLoading) {
        a.dismiss().then(() => console.log('abort presenting'));
      }
    });
  });
}

async dismiss() {
  this.isLoading = false;
  return await this.loadingController.dismiss().then(() => console.log('dismissed'));
}
}

 
