import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalsearchPage } from './globalsearch.page';

describe('GlobalsearchPage', () => {
  let component: GlobalsearchPage;
  let fixture: ComponentFixture<GlobalsearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalsearchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalsearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
